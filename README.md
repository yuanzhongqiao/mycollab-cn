<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><h1 tabindex="-1" dir="auto"><a id="user-content-free-open-source-project-management-software" class="anchor" aria-hidden="true" tabindex="-1" href="#free-open-source-project-management-software"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">免费、开源项目管理软件</font></font></h1>
<p dir="auto"><a href="https://www.gnu.org/licenses/agpl-3.0.en.html" rel="nofollow"><img src="https://camo.githubusercontent.com/41ee465e0d75651e521e0309895d44483ca56b1cf051c7764a736fd93c008e04/687474703a2f2f696d672e736869656c64732e696f2f62616467652f4c6963656e73652d4147504c76332d6f72616e67652e737667" alt="执照" data-canonical-src="http://img.shields.io/badge/License-AGPLv3-orange.svg" style="max-width: 100%;"></a> <animated-image data-catalyst=""><a href="https://www.openhub.net/p/mycollab" rel="nofollow" data-target="animated-image.originalLink"><img src="https://camo.githubusercontent.com/6401b87274ea59d436b2b89567909a2decbb930ea95022560aa2afcac39d756e/68747470733a2f2f7777772e6f70656e6875622e6e65742f702f6d79636f6c6c61622f776964676574732f70726f6a6563745f7468696e5f62616467652e676966" alt="项目统计" data-canonical-src="https://www.openhub.net/p/mycollab/widgets/project_thin_badge.gif" style="max-width: 100%; display: inline-block;" data-target="animated-image.originalImage"></a>
 <a href="https://travis-ci.org/MyCollab/mycollab" rel="nofollow"><img src="https://camo.githubusercontent.com/afe15119ec92a027853dd8f2c3fd624b42f0676d2979583b9f7d5b6c21248241/68747470733a2f2f7472617669732d63692e6f72672f4d79436f6c6c61622f6d79636f6c6c61622e737667" alt="建造" data-canonical-src="https://travis-ci.org/MyCollab/mycollab.svg" style="max-width: 100%;"></a>
<a href="https://docs.mycollab.com/" rel="nofollow"><img src="https://camo.githubusercontent.com/0eca0ace0d43d5970e7c605136d1529d6698cd9e5cbb39fc78c6ca6f240f6394/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f56657273696f6e2d372e302e332d627269676874677265656e2e737667" alt="版本" data-canonical-src="https://img.shields.io/badge/Version-7.0.3-brightgreen.svg" style="max-width: 100%;"></a>
<a href="https://github.com/MyCollab/mycollab/releases"><img src="https://camo.githubusercontent.com/4064d7dca6f09de5f907d7d34bee0a4407a8e69819f0da392f103681f0ef6319/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f646f776e6c6f6164732f4d79436f6c6c61622f6d79636f6c6c61622f746f74616c2e737667" alt="吉图布" data-canonical-src="https://img.shields.io/github/downloads/MyCollab/mycollab/total.svg" style="max-width: 100%;"></a></p>
<h2 tabindex="-1" dir="auto"><a id="user-content-introduction" class="anchor" aria-hidden="true" tabindex="-1" href="#introduction"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">介绍</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MyCollab 是免费的开源项目管理软件。</font><font style="vertical-align: inherit;">直观的用户界面、丰富的功能、高性能和稳定是与市场上各种流行工具（例如Redmine、Bugzilla、Mantis等）相比的优势。该开源被包含在值得信赖的商业产品中，部署在数百家公司的服务器上。</font></font></p>
<table>
  <tbody><tr>
    <td align="center">
      <a href="https://c2.staticflickr.com/8/7836/33297801958_8c403afca8_o.png" title="项目仪表板" rel="nofollow">
        <img src="https://camo.githubusercontent.com/da5fd618da8132ad9c226c983dbad002a5425bd886ab4001a57cd04b439d3fcb/68747470733a2f2f63322e737461746963666c69636b722e636f6d2f382f373833362f33333239373830313935385f633339353865393462615f6e2e6a7067" alt="项目仪表板" data-canonical-src="https://c2.staticflickr.com/8/7836/33297801958_c3958e94ba_n.jpg" style="max-width: 100%;">
      </a>
      <br>
      <em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">项目仪表板</font></font></em>
    </td>
    <td align="center">
      <a href="https://c2.staticflickr.com/8/7918/47173080821_3352d05e2b_o.png" title="票务仪表板" rel="nofollow">
        <img src="https://camo.githubusercontent.com/4932c8e37139c937ccbde3f8abf62b9d731b7aefc0f839a228a4aa9ecabb715d/68747470733a2f2f63322e737461746963666c69636b722e636f6d2f382f373931382f34373137333038303832315f663663303932383232655f6e2e6a7067" alt="票务仪表板" data-canonical-src="https://c2.staticflickr.com/8/7918/47173080821_f6c092822e_n.jpg" style="max-width: 100%;">
      </a>
      <br>
      <em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">票务仪表板</font></font></em>
    </td>
    <td align="center">
    <a href="https://c2.staticflickr.com/8/7868/46259674665_52e5d9ec03_o.png" title="看板" rel="nofollow">
      <img src="https://camo.githubusercontent.com/4c2d4478b6af5f332c4139f4f57e8ebda0517a1ace79d346cdfc81d58f8d6d2f/68747470733a2f2f63322e737461746963666c69636b722e636f6d2f382f373836382f34363235393637343636355f633830613063313561375f6e2e6a7067" alt="看板" data-canonical-src="https://c2.staticflickr.com/8/7868/46259674665_c80a0c15a7_n.jpg" style="max-width: 100%;">
    </a>
      <br>
      <em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">看板</font></font></em>
    </td>
  </tr>
  <tr>
    <td align="center">
    <a href="https://c2.staticflickr.com/8/7874/46259716315_bd4269858d_o.png" title="任务视图" rel="nofollow">
        <img src="https://camo.githubusercontent.com/dc3ba765b3aadea61aeb3be72b4c4a633f290b3dffd2b6ed7591f0dc70cdb045/68747470733a2f2f63322e737461746963666c69636b722e636f6d2f382f373837342f34363235393731363331355f343430343761663835655f6e2e6a7067" alt="任务视图" data-canonical-src="https://c2.staticflickr.com/8/7874/46259716315_44047af85e_n.jpg" style="max-width: 100%;">
      </a>
      <br>
      <em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">任务视图</font></font></em>
    </td>
    <td align="center">
      <a href="https://c2.staticflickr.com/8/7896/47173858441_f2395a1b7d_o.png" title="会员" rel="nofollow">
        <img src="https://camo.githubusercontent.com/d752f35ce791b397141a10a72e12880cca900cb04caffaf7c5da7bbc5a521d72/68747470733a2f2f63322e737461746963666c69636b722e636f6d2f382f373839362f34373137333835383434315f336234633737393930665f6e2e6a7067" alt="会员" data-canonical-src="https://c2.staticflickr.com/8/7896/47173858441_3b4c77990f_n.jpg" style="max-width: 100%;">
      </a>
      <br>
      <em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">会员</font></font></em>
    </td>
    <td align="center">
      <a href="https://c2.staticflickr.com/8/7862/40209055153_0a16241b1b_o.png" title="设置" rel="nofollow">
        <img src="https://camo.githubusercontent.com/dd6bdbaafab51fa675a4a8002df47ef6980476be761283e801a08b6c0d39848e/68747470733a2f2f63322e737461746963666c69636b722e636f6d2f382f373836322f34303230393035353135335f353461343237653539335f6e2e6a7067" alt="设置" data-canonical-src="https://c2.staticflickr.com/8/7862/40209055153_54a427e593_n.jpg" style="max-width: 100%;">
      </a>
      <br>
      <em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设置</font></font></em>
    </td>
  </tr>
</tbody></table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新功能、增强功能和更新会定期出现。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">始终欢迎拉取请求和错误报告！</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请访问我们的网站</font></font><a href="https://www.mycollab.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.mycollab.com/</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以免费试用高级服务。</font></font></p>
<h2 tabindex="-1" dir="auto"><a id="user-content-features" class="anchor" aria-hidden="true" tabindex="-1" href="#features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MyCollab 提供丰富的项目管理、客户管理模块和在线协作方式等功能。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">项目管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">活动流和审核日志记录</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">看板</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">路线图视图</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">问题管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">任务和依赖关系管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">里程碑</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">时间跟踪（仅限高级用户）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发票管理（仅限高级用户）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">风险管理（仅限高级用户）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人员和权限管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">报告</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们在日常工作中使用 MyCollab 来管理客户信息、项目。</font><font style="vertical-align: inherit;">它部署在我们高级用户的生产环境中，我们也支持多个组织在他们的服务器上部署这个社区版本。</font><font style="vertical-align: inherit;">我们对开源版本的管理与我们对高级产品的管理类似，事实上，它们都使用相同的代码库结构。</font><font style="vertical-align: inherit;">因此，请随意在您的商业工作中使用它！</font></font></p>
<h2 tabindex="-1" dir="auto"><a id="user-content-system-requirements" class="anchor" aria-hidden="true" tabindex="-1" href="#system-requirements"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">系统要求</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MyCollab 需要正在运行的 Java 运行时环境（8 或更高版本），Java 命令应出现在 PATH 环境和 MySQL 中（建议支持 InnoDB）。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Java 运行时环境 8+：MyCollab 可以在任何 JVM 兼容平台（例如 Oracle JRE 或 OpenJDK）上运行。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MySQL数据库，版本5.6+：建议使用更高版本</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最低 1 GB RAM，建议 2 GB RAM</font></font></li>
</ul>
<h2 tabindex="-1" dir="auto"><a id="user-content-installation" class="anchor" aria-hidden="true" tabindex="-1" href="#installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></h2>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下载 MyCollab 二进制文件 - </font></font><a href="https://www.mycollab.com/self-hosted/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.mycollab.com/self-hosted/</font></font></a></li>
<li><font style="vertical-align: inherit;"><a href="https://docs.mycollab.com/getting-started/installation/" rel="nofollow"><font style="vertical-align: inherit;">请遵循https://docs.mycollab.com/getting-started/installation/</font></a><font style="vertical-align: inherit;">上的安装指南</font></font><a href="https://docs.mycollab.com/getting-started/installation/" rel="nofollow"><font style="vertical-align: inherit;"></font></a></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您需要了解更多 MyCollab 高级配置设置，请访问链接</font></font><a href="https://docs.mycollab.com/getting-started/configuration/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://docs.mycollab.com/getting-started/configuration/</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">您将在几分钟内完成阅读和理解。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您想自定义 MyCollab，以下链接对您有用：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 IntelliJ 设置 MyCollab 项目</font></font><a href="https://docs.mycollab.com/development/setup-mycollab-projects-with-intellij-ide/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://docs.mycollab.com/development/setup-mycollab-projects-with-intellij-ide/</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何自定义 MyCollab </font></font><a href="https://docs.mycollab.com/development/customize-mycollab/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://docs.mycollab.com/development/customize-mycollab/</font></font></a></li>
</ul>
<h2 tabindex="-1" dir="auto"><a id="user-content-support" class="anchor" aria-hidden="true" tabindex="-1" href="#support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">联系 MyCollab 团队：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们不断增长的常见问题解答</font></font><a href="https://docs.mycollab.com/faq/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://docs.mycollab.com/faq/</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们的帮助页面</font></font><a href="https://mycollab.userecho.com/en/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://support.mycollab.com/</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们的网络表单</font></font><a href="https://www.mycollab.com/contact/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.mycollab.com/contact/</font></font></a></li>
</ul>
<h2 tabindex="-1" dir="auto"><a id="user-content-license--author" class="anchor" aria-hidden="true" tabindex="-1" href="#license--author"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">许可和作者</font></font></h2>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MyCollab 社区已获得 Affero GPL v3 许可。</font><font style="vertical-align: inherit;">有关许可条款，请参阅</font></font><a href="https://www.gnu.org/licenses/agpl-3.0.en.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.gnu.org/licenses/agpl-3.0.en.html</font></font></a></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><a href="https://www.mycollab.com" rel="nofollow"><font style="vertical-align: inherit;">您可以在网站https://www.mycollab.com</font></a><font style="vertical-align: inherit;">上尝试 MyCollab 点播版</font></font><a href="https://www.mycollab.com" rel="nofollow"><font style="vertical-align: inherit;"></font></a></p>
</li>
</ul>
</article></div>
